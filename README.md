Mega Store
================

restore database from megastore.sql file
and change the database configuration in 'persistence-mysql.properties'

To Build
===========
mvn clean install

To run
=============
mvn jetty:run


End Points
============
1. Get All products existing products:
GET http://localhost:7070/api/rest/products?size=100

2. To Add new product:

POST http://localhost:7070/api/rest/products
{
   
    "name" : "Test",
    "description" : "dummy description"
}
3. To Add prices of ant existing product

POST http://localhost:7070/api/rest/products/{Product-ID}/prices?size=12
{

    "price": 4.56,
    "startDate": "2016-01-12",
    "stopDate": "2016-06-20",
    "currency": "INR"
}
 4. To get all products with prices (Returns only enabled prices)

GET http://localhost:7070/api/rest/products/{Product-ID}/prices?size=12
