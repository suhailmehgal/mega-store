package com.spring.megastore.utils;

import com.spring.megastore.enums.Roles;
import com.spring.megastore.persistence.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserUtil.class);
	public static User getUser() {
		User user = null;
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			//TODO: Can't type cast from Spring user to own,
			// may be needs to implement all spring user property to own user
			//user = (User) auth.getPrincipal();
			org.springframework.security.core.userdetails.User springUser = (org.springframework.security.core.userdetails.User) auth.getPrincipal();

			if(user == null) {
				user = new User();
				user.setEmail(springUser.getUsername());
			}
		} catch (ClassCastException e) {
			LOGGER.error("Problem while getting user obj {} {} ", e, e.getStackTrace());
		}
		return user;
	}

	public static boolean isUserAdmin(){
		if(getUser().getRole().getRole().equalsIgnoreCase(Roles.ROLE_PRODUCT_MANAGER.name())){
			return true;
		}
		return false;
	}

	public static boolean isUserAgent(){
		if(getUser().getRole().getRole().equalsIgnoreCase(Roles.ROLE_USER.name())){
			return true;
		}
		return false;
	}

	public static long getUserId(){
		return getUser().getId();
	}

	public static String getUserEmail() {
		return getUser().getEmail();
	}

}
