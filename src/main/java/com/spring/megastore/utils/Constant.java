package com.spring.megastore.utils;

import com.google.common.collect.ImmutableMap;
import java.text.SimpleDateFormat;
import java.util.Map;

public class Constant {

	public static final String RESOURCE_ID = "restService";
	public static final String DATE_FORMAT = "dd-MM-yyyy";
	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT);
	public static final String BASE_PACKAGE 				= "com.spring.megastore";
	public static final String CONFIG_LOCATION 				= "com.spring.megastore.config";
	public static final String MAPPING_URL 					= "/";
	public static final String PERSISTENCE_MODEL_PACKAGE 	= "com.spring.megastore.persistence.model";
	public static final String PERSISTENCE_PACKAGE 			= "com.spring.megastore.persistence";
	public static final int DEFAULT_SIZE = 10;
	public static final int DEFAULT_START_INDEX = 0;

	public static final Map<String,Object> successResponse = ImmutableMap.<String, Object>builder()
																		.put("acknowledge", true)
																		.build();

	public static final Map<String,Object> failureResponse = ImmutableMap.<String, Object>builder()
																		.put("acknowledge", false)
																		.build();
	public static final String CLIENT_ID = "clientId";
	public static final String DEFAULT_CLIENT_ID = "MyApp";
	public static final String SECRET = "secret";
	public static final String PASSWORD = "password";
	public static final String USER_NAME = "userName";
	public static final int DEFAULT_TOKEN_VALIDITY = 600;
	public static final String WEB_SERVER_REDIRECT_URI = "redirectUri";
	public static final String ERROR_CODE = "errorCode";
	public static final String ERROR_MESSAGE = "errorMessage";
}
