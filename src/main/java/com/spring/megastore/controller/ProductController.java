package com.spring.megastore.controller;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.spring.megastore.persistence.model.Price;
import com.spring.megastore.persistence.model.Product;
import com.spring.megastore.service.IPriceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.spring.megastore.service.IProductService;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static com.spring.megastore.utils.Constant.*;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping(value = "/api/rest", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private IProductService productService;

	@Autowired
	private IPriceService priceService;

	/**
	 * Adds new product
	 * @return
     */
	@RequestMapping(value = "/products", method = RequestMethod.POST)
	public
	@ResponseBody ResponseEntity addProduct(@RequestBody Product product) {
		productService.create(product);
		return new ResponseEntity<>(successResponse, OK);
	}

	/**
	 * Returns list of the products equals to the size parameter
	 * default size is 10
	 * @return
	 */
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public
	@ResponseBody
	ResponseEntity<List<Product>> getAllProducts(@RequestParam(value = "size", required = false) Integer size) {

		Optional<Integer> pageSize = Optional.ofNullable(size);
		return new ResponseEntity<>(productService.listPaginatedProducts(DEFAULT_START_INDEX, pageSize.isPresent() ? pageSize.get() : DEFAULT_SIZE), OK);
	}

	/**
	 * Updates price of a existing product
	 * @return
	 */
	@RequestMapping(value = "/products/{productId}/prices", method = RequestMethod.POST)
	public
	@ResponseBody ResponseEntity updatePrices(@PathVariable("productId") Long productId,
											  @RequestBody Price price
											  ) {

		Preconditions.checkNotNull(productId, "Product id can't be null or empty");

		Optional<Product> product = Optional.ofNullable(productService.findOne(productId));

		if(product.isPresent()) {
			price.setProductId(productId);
			priceService.create(price);
			return new ResponseEntity<>(successResponse, OK);
		} else {

			LOGGER.error("Product Id is invalid :{}", productId);
			final Map<String,Object> response = ImmutableMap.<String,Object>builder()
															.putAll(failureResponse)
															.put(ERROR_MESSAGE,"Product id not found")
															.put(ERROR_CODE, 404)
															.build();
			return new ResponseEntity<>(response, OK);
		}
	}

	/**
	 * Returns list of prices equals to the size parameter
	 * default size is 10
	 * @return
	 */
	@RequestMapping(value = "/products/{productId}/prices", method = RequestMethod.GET)
	public
	@ResponseBody ResponseEntity<List<Price>> getPrices(@PathVariable("productId") Integer productId, @RequestParam(value = "size", required = false) Integer size) {

		Preconditions.checkNotNull(productId, "Product id can't be null or empty");
        Optional<Integer> pageSize = Optional.ofNullable(size);
		return new ResponseEntity<>(priceService.listPaginatedPrices(DEFAULT_START_INDEX, pageSize.isPresent() ? pageSize.get() : DEFAULT_SIZE, productId), OK);
	}

	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE)
	public
	@ResponseBody ResponseEntity removeProduct(@PathVariable("id") Integer productId) {

		Preconditions.checkNotNull(productId);

		productService.deleteById(productId);

		return new ResponseEntity<>(successResponse, OK);
	}
}
