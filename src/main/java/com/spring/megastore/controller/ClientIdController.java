package com.spring.megastore.controller;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.spring.megastore.persistence.model.OauthClientDetails;
import com.spring.megastore.persistence.model.User;
import com.spring.megastore.service.IOauthClientDetailsService;
import com.spring.megastore.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;
import static com.spring.megastore.utils.Constant.*;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(value = "/generate-client", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientIdController {

    @Autowired
    private IUserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IOauthClientDetailsService oauthClientDetailsService;

    /**
     * Creates Client Id and secret code for the user to access OAuth2.0
     * @param map
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity generateClient(@RequestBody Map<String, Object> map) {

        Preconditions.checkNotNull(map);

        String userName = Preconditions.checkNotNull(map.get(USER_NAME)).toString();
        String password = Preconditions.checkNotNull(map.get(PASSWORD)).toString();
        String clientId = map.getOrDefault(CLIENT_ID, DEFAULT_CLIENT_ID).toString();

        ImmutableMap.Builder<String, Object> response = ImmutableMap.<String, Object>builder();

        User user = new User();
        user.setUsername(userName);
        user.setPassword(passwordEncoder.encode(password));

        String secret = UUID.randomUUID().toString();

        if(validateUser(user)) {
            OauthClientDetails clientDetails = new OauthClientDetails();

            clientDetails.setClientId(clientId);
            clientDetails.setClientSecret(secret);
            clientDetails.setAccessTokenValidity(DEFAULT_TOKEN_VALIDITY);
            clientDetails.setResourceIds(RESOURCE_ID);
            clientDetails.setWebServerRedirectUri(map.getOrDefault(WEB_SERVER_REDIRECT_URI,"").toString());

            oauthClientDetailsService.create(clientDetails);

            response.putAll(successResponse)
                    .put(CLIENT_ID, clientId)
                    .put(SECRET, secret);

            return new ResponseEntity<>(response.build(), OK);
        } else {

            response.put(ERROR_CODE, 401)
                    .put(ERROR_MESSAGE,"Username or password is wrong")
                    .putAll(failureResponse);
            return new ResponseEntity<>(response.build(), OK);
        }
    }

    /**
     * Validates user from username and password
     * @param user
     * @return
     */
    private boolean validateUser(User user) {
       return userService.loadUserByUsername(user.getUsername()).getPassword().equals(user.getPassword());
    }

}
