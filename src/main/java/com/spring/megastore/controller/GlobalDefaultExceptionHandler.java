package com.spring.megastore.controller;

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import static com.spring.megastore.utils.Constant.ERROR_CODE;
import static com.spring.megastore.utils.Constant.ERROR_MESSAGE;
import static com.spring.megastore.utils.Constant.failureResponse;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

	/**
	 * Handles all server side exceptions and returns error response
	 * @param request
	 * @param exception
     * @return
     */
	 @ExceptionHandler(value = {Exception.class})
	 public @ResponseBody
	 ResponseEntity defaultErrorHandler(HttpServletRequest request, Exception exception) {

		 LOGGER.error("Exception occurred \n Logs may come to rescue: ", exception);
		 Map<String,Object> response = Maps.newHashMap();
		 response.put(ERROR_CODE, 500);
		 response.put(ERROR_MESSAGE, "Something went wrong");
		 response.putAll(failureResponse);
		 return new ResponseEntity<>(response, INTERNAL_SERVER_ERROR);
	 }


	@ExceptionHandler(value = {UsernameNotFoundException.class})
	@ResponseStatus(NOT_FOUND)
	public @ResponseBody ResponseEntity notFound() {
		Map<String,Object> response = Maps.newHashMap();
		response.put(ERROR_CODE, 404);
		response.put(ERROR_MESSAGE, "requested url doesn't exist");
		response.putAll(failureResponse);
		return new ResponseEntity<>(response, NOT_FOUND);
	}

}
