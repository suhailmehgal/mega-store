package com.spring.megastore.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;


public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
//	AbstractSecurityWebApplicationInitializer, it will load the springSecurityFilterChain automatically.
}