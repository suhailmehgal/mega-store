package com.spring.megastore.enums;

/**
 *
 * @author Suhail
 */

public enum Roles {
    USER,
    PRODUCT_MANAGER,
    ROLE_PRODUCT_MANAGER,
    ROLE_USER
}
