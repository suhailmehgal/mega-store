package com.spring.megastore.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by sohail on 17/3/16.
 */
@Entity
@Table(name = "url_access_history")
public class UrlAccessHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "access_token", nullable = false)
    private byte[] accessToken;

    @Column(name = "method", nullable = false)
    private String method;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "parameters", nullable = false)
    private String parameters;

    @Column(name = "access_time", nullable = false)
    private Timestamp accessTme;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public byte[] getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(byte[] accessToken) {
        this.accessToken = accessToken;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public Timestamp getAccessTme() {
        return accessTme;
    }

    public void setAccessTme(Timestamp accessTme) {
        this.accessTme = accessTme;
    }

    @Override
    public String toString() {
        return "UrlAccessHistory{" +
                "id=" + id +
                ", accessToken=" + Arrays.toString(accessToken) +
                ", method='" + method + '\'' +
                ", url='" + url + '\'' +
                ", parameters='" + parameters + '\'' +
                ", accessTme=" + accessTme +
                '}';
    }
}
