package com.spring.megastore.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by sohail on 17/3/16.
 */
@Entity
@Table(name = "oauth_code")
public class OauthCode implements Serializable {
    private String code;
    private byte[] authentication;

    @Id
    @Basic
    @Column(name = "code", nullable = false, length = 256)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "authentication", nullable = false)
    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    @Override
    public String toString() {
        return "OauthCode{" +
                "code='" + code + '\'' +
                ", authentication=" + Arrays.toString(authentication) +
                '}';
    }
}
