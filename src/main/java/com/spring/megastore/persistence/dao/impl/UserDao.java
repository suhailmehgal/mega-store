package com.spring.megastore.persistence.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import com.spring.megastore.persistence.dao.IUserDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.User;

@Repository
@SuppressWarnings("unchecked")
public class UserDao extends AbstractHibernateDao<User> implements IUserDao {

	public UserDao() {
        super();
        setClazz(User.class);
    }

	@Override
	public List<User> listPaginatedUsers(int startIndex,int numberOfRecordsToFetch) {
		Query query = getCurrentSession().createQuery("SELECT user.id AS id,user.firstName AS firstName,user.lastName AS lastName,user.email AS email,user.role AS role FROM " +  getClazz().getName() + " user WHERE enabled = :enabled");
		query.setParameter("enabled", true);
		query.setFirstResult(startIndex);
		query.setMaxResults(numberOfRecordsToFetch);
		query.setResultTransformer(new AliasToBeanResultTransformer(getClazz()));
		return query.list();

	}
	
	@Override
	public List<User> getUserById(String id) {
		Query query = getCurrentSession().createQuery("FROM " +  getClazz().getName() + " WHERE email = :email");
		query.setParameter("email", id);
		return query.list();
	}

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return (UserDetails) getCurrentSession().createCriteria(User.class).add(Restrictions.eq("userName", username)).uniqueResult();
	}
}