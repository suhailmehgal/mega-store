package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IOAuthAccessTokenDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.OauthAccessToken;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class OAuthAccessTokenDao extends AbstractHibernateDao<OauthAccessToken> implements IOAuthAccessTokenDao {

    public OAuthAccessTokenDao() {
        super();
        setClazz(OauthAccessToken.class);
    }

}
