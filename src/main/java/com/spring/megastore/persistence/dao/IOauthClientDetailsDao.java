package com.spring.megastore.persistence.dao;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.OauthClientDetails;

/**
 * Created by sohail on 18/3/16.
 */
public interface IOauthClientDetailsDao extends IOperations<OauthClientDetails> {
}
