package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IOauthCodeDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.OauthCode;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class OauthCodeDao extends AbstractHibernateDao<OauthCode> implements IOauthCodeDao {

    public OauthCodeDao() {
        super();
        setClazz(OauthCode.class);
    }
}
