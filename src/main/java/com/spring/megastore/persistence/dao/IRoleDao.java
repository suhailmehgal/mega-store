package com.spring.megastore.persistence.dao;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Role;

public interface IRoleDao extends IOperations<Role>{
	// Add method which are not common for other DAO
}
