package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.model.Product;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.spring.megastore.persistence.dao.IProductDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;

import java.util.List;

@Repository
public class ProductDao extends AbstractHibernateDao<Product> implements IProductDao {

	public ProductDao() {
        super();
        setClazz(Product.class);
    }

    @Override
    public List<Product> listPaginatedProducts(int startIndex, int numberOfRecordsToFetch) {
        Query query = getCurrentSession().createQuery(" FROM " +  getClazz().getName())
                            .setFirstResult(startIndex)
                            .setMaxResults(numberOfRecordsToFetch);
        return query.list();
    }
}