package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IOauthRefreshTokenDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.OauthRefreshToken;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class OauthRefreshTokenDao extends AbstractHibernateDao<OauthRefreshToken> implements IOauthRefreshTokenDao {

    public OauthRefreshTokenDao() {
        super();
        setClazz(OauthRefreshToken.class);
    }

}
