package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.ILoginHistoryDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.LoginHistory;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class LoginHistoryDao extends AbstractHibernateDao<LoginHistory> implements ILoginHistoryDao {

    public LoginHistoryDao() {
        super();
        setClazz(LoginHistory.class);
    }
}
