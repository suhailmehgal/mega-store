package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IAuthoritiesDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.Authorities;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class AuthoritiesDao extends AbstractHibernateDao<Authorities> implements IAuthoritiesDao {

    public AuthoritiesDao() {
        super();
        setClazz(Authorities.class);
    }

}
