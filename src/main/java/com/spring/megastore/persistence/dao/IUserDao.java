package com.spring.megastore.persistence.dao;

import java.util.List;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface IUserDao extends IOperations<User>{
	List<User> listPaginatedUsers(int startIndex , int numberOfRecordsToFetch);
	UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;
	List<User> getUserById(String id);
}

