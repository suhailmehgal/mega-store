package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IPriceDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.Price;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Created by sohail on 10/3/16.
 */
@Repository
public class PriceDao extends AbstractHibernateDao<Price> implements IPriceDao {

    public PriceDao() {
        super();
        setClazz(Price.class);
    }

    @Override
    public List<Price> listPaginatedPrices(int startIndex, int numberOfRecordsToFetch, int productId) {
        Query query = getCurrentSession().createQuery(" FROM " +  getClazz().getName() + " WHERE productId = :productId AND isActive=:isActive")
                                        .setParameter("productId", (long)productId)
                                        .setParameter("isActive", true)
                                        .setFirstResult(startIndex)
                                        .setMaxResults(numberOfRecordsToFetch);
        return query.list();
    }

}
