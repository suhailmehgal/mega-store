package com.spring.megastore.persistence.dao;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Price;

import java.util.List;

/**
 * Created by sohail on 10/3/16.
 */
public interface IPriceDao extends IOperations<Price>{
    List<Price> listPaginatedPrices(int startIndex , int numberOfRecordsToFetch, int productId);
}
