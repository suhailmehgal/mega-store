package com.spring.megastore.persistence.dao;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Product;

import java.util.List;


public interface IProductDao extends IOperations<Product>{
    List<Product> listPaginatedProducts(int startIndex , int numberOfRecordsToFetch);
}
