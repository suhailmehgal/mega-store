package com.spring.megastore.persistence.dao.impl;

import org.springframework.stereotype.Repository;

import com.spring.megastore.persistence.dao.IRoleDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.Role;

@Repository
public class RoleDao extends AbstractHibernateDao<Role> implements IRoleDao {

	public RoleDao() {
        super();
        setClazz(Role.class);
    }
}