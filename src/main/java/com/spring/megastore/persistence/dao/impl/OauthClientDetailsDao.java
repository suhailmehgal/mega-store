package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IOauthClientDetailsDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.OauthClientDetails;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class OauthClientDetailsDao extends AbstractHibernateDao<OauthClientDetails> implements IOauthClientDetailsDao {

    public OauthClientDetailsDao() {
        super();
        setClazz(OauthClientDetails.class);
    }

}
