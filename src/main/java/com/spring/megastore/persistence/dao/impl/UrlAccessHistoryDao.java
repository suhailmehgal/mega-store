package com.spring.megastore.persistence.dao.impl;

import com.spring.megastore.persistence.dao.IUrlAccessHistoryDao;
import com.spring.megastore.persistence.dao.common.AbstractHibernateDao;
import com.spring.megastore.persistence.model.UrlAccessHistory;
import org.springframework.stereotype.Repository;

/**
 * Created by sohail on 18/3/16.
 */
@Repository
public class UrlAccessHistoryDao extends AbstractHibernateDao<UrlAccessHistory> implements IUrlAccessHistoryDao {

    public UrlAccessHistoryDao() {
        super();
        setClazz(UrlAccessHistory.class);
    }

}
