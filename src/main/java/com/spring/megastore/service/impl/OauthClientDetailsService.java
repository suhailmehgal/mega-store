package com.spring.megastore.service.impl;

import com.spring.megastore.persistence.dao.IOauthClientDetailsDao;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.OauthClientDetails;
import com.spring.megastore.service.IOauthClientDetailsService;
import com.spring.megastore.service.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sohail on 18/3/16.
 */
@Service
public class OauthClientDetailsService extends AbstractService<OauthClientDetails> implements IOauthClientDetailsService {

    @Autowired
    private IOauthClientDetailsDao dao;

    public OauthClientDetailsService() {
        super();
    }

    @Override
    protected IOperations<OauthClientDetails> getDao() {
        return dao;
    }
}
