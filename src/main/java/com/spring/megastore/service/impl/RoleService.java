package com.spring.megastore.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.megastore.persistence.dao.IRoleDao;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Role;
import com.spring.megastore.service.IRoleService;
import com.spring.megastore.service.common.AbstractService;

@Service
public class RoleService extends AbstractService<Role> implements IRoleService {

	@Autowired
    private IRoleDao dao;

    public RoleService() {
        super();
    }

	@Override
	protected IOperations<Role> getDao() {
		 return dao;
	}

	@Override
	public Map<String, String> getAllRole() {
		List<Role> roleList = getDao().findAll();
		Map<String,String> roleMap = new HashMap<>();
		for(Role role : roleList){
			roleMap.put(String.valueOf(role.getId()), role.getRoleName());
		}
		return roleMap;
	}

}
