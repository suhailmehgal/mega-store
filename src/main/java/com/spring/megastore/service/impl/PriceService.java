package com.spring.megastore.service.impl;

import com.spring.megastore.persistence.dao.IPriceDao;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Price;
import com.spring.megastore.service.IPriceService;
import com.spring.megastore.service.common.AbstractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sohail on 10/3/16.
 */
@Service
public class PriceService extends AbstractService<Price> implements IPriceService {

    @Autowired
    private IPriceDao dao;

    public PriceService() {
        super();
    }

    @Override
    protected IOperations<Price> getDao() {
        return dao;
    }

    @Override
    public List<Price> listPaginatedPrices(int startIndex, int numberOfRecordsToFetch, int productId) {
        return dao.listPaginatedPrices(startIndex, numberOfRecordsToFetch, productId);
    }
}
