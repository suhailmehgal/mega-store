package com.spring.megastore.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.spring.megastore.persistence.dao.IUserDao;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.User;
import com.spring.megastore.service.IUserService;
import com.spring.megastore.service.common.AbstractService;

@Service
public class UserService extends AbstractService<User> implements IUserService {

	@Autowired
    private IUserDao dao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

    public UserService() {
        super();
    }

	@Override
	protected IOperations<User> getDao() {
		 return dao;
	}

	@Override
    public void create(final User entity) {
		entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        dao.create(entity);
    }

	@Override
	public List<User> listPaginatedUsers(int startIndex,int numberOfRecordsToFetch) {
		return dao.listPaginatedUsers(startIndex, numberOfRecordsToFetch);
	}
	
	@Override
	public User getUserById(String id) {
		List<User> list = dao.getUserById(id);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public User setUserAccountAccessControl(User user) {
		user.setAccountNonExpired(true);
		user.setAccountNonLocked(true);
		user.setCredentialsNonExpired(true);
		user.setEnabled(true);
		return user;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return dao.loadUserByUsername(email);
	}
}
