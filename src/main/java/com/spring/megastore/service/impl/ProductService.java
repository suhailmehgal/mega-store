package com.spring.megastore.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.spring.megastore.persistence.dao.IProductDao;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Product;
import com.spring.megastore.service.IProductService;
import com.spring.megastore.service.common.AbstractService;
import java.util.List;

@Service
public class ProductService extends AbstractService<Product> implements IProductService {

	@Autowired
    private IProductDao dao;

    public ProductService() {
        super();
    }

	@Override
	protected IOperations<Product> getDao() {
		 return dao;
	}

	@Override
	public List<Product> listPaginatedProducts(int startIndex, int numberOfRecordsToFetch) {
		return dao.listPaginatedProducts(startIndex, numberOfRecordsToFetch);
	}
}