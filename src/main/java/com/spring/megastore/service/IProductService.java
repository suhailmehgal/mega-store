package com.spring.megastore.service;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Product;
import java.util.List;

public interface IProductService extends IOperations<Product>{
	// Add method which are not common for other Service
    List<Product> listPaginatedProducts(int startIndex , int numberOfRecordsToFetch);
}