package com.spring.megastore.service;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.OauthClientDetails;

/**
 * Created by sohail on 18/3/16.
 */
public interface IOauthClientDetailsService extends IOperations<OauthClientDetails> {
}
