package com.spring.megastore.service;

import java.util.List;

import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface IUserService extends IOperations<User> , UserDetailsService {
	List<User> listPaginatedUsers(int startIndex , int numberOfRecordsToFetch);
	User getUserById(String id);
	User setUserAccountAccessControl(User user);
}
