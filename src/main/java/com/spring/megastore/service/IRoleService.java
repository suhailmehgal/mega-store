package com.spring.megastore.service;

import java.util.Map;
import com.spring.megastore.persistence.dao.common.IOperations;
import com.spring.megastore.persistence.model.Role;

public interface IRoleService extends IOperations<Role>{
	Map<String,String> getAllRole();
}
