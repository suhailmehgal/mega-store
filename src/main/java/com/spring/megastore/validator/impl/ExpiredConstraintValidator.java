package com.spring.megastore.validator.impl;

import com.spring.megastore.validator.Expired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.ParseException;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.spring.megastore.utils.Constant.SIMPLE_DATE_FORMAT;

/**
 * Created by sohail on 9/3/16.
 */
public class ExpiredConstraintValidator implements ConstraintValidator<Expired, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpiredConstraintValidator.class);

    @Override
    public void initialize(Expired expired) {
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {

        if(!isNullOrEmpty(date)) {
            try {
                SIMPLE_DATE_FORMAT.parse(date);
                return true;
            } catch (ParseException e) {
                LOGGER.error("Error in date parsing: {}", date);
                return false;
            }
        }
        return false;
    }
}
