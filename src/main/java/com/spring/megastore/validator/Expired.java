package com.spring.megastore.validator;

import com.spring.megastore.validator.impl.ExpiredConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by sohail on 9/3/16.
 */
@Target(ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ExpiredConstraintValidator.class)
public @interface Expired {
    String startDate() default "";
    String endDate() default "";
    String message() default "{}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
